module Trakrgem
  class Setup
    extend ::Trakrgem::Base

    def self.set_api_key(key)
      file = File.open("#{Dir.home}/.trakrrc", 'w+')
      file.puts("api_key:#{key}")
      file.close
    end

    def self.login
      STDOUT.puts 'You must login to start using Trakr'
      email = ask('E-mail?')
      password = ask('Password? (typing will be hidden)') { |q| q.echo = false }
      request = Utilities.api_post('users/login', {email: email, password: password })
      response = JSON.parse(request.response_body)
      if response['login'] == 'successful'
        set_api_key(response['api_key'])
        STDOUT.puts "You have been logged in"
      else
        STDOUT.puts "Login Unsucessful, please try again"
      end
    end
  end
end
