module Trakrgem
  class Activity
    extend ::Trakrgem::Base

    def self.last
      api_key_set?
      activity = Utilities.api_get('activities/latest', { api_key: api_key })
      json = Utilities.parse_json(activity.response_body)['activity']
      STDOUT.puts Utilities.format_activity(json)
    end

    def self.post(label, description)
      api_key_set?
      post = Utilities.api_post('activities', { label: label, description: description, api_key: api_key })
      json = Utilities.parse_json(post.response_body)
      STDOUT.puts "Created: " + Utilities.format_activity(json['activity'], true)
    end

    def self.list
      api_key_set?
      activities = Utilities.api_get('activities', { api_key: api_key })
      json = Utilities.parse_json(activities.response_body)
      tp json['activities'], 'id', 'label', 'description', 'logged_at' => lambda{|l| Time.parse(l['logged_at']).strftime("%m %b %y %I:%M%P") }
    end
  end
end
