module Trakrgem
  module Base
    def api_key
      File.read("#{Dir.home}/.trakrrc").gsub('api_key:','').strip
    end

    def api_key_set?
      begin
      if !File.exists?("#{Dir.home}/.trakkrc") && api_key.nil?
        raise "Please log in."
      end
      rescue
        raise "Please log in."
      end
    end
  end
end
