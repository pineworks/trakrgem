module Trakrgem
  class Utilities
    extend ::Trakrgem::Base

    def self.api_url(path)
      "http://trakrmasterb980.ninefold-apps.com/api/#{path}"
    end

    def self.api_post(path, params = {})
      Typhoeus.post(Utilities.api_url(path), body: params )
    end

    def self.api_get(path, params = {})
      Typhoeus.get(Utilities.api_url(path), body: params )
    end

    def self.parse_json(content)
      JSON.parse(content)
    end

    def self.format_activity(json, new = false)
      if new
        "#{json['label']} - #{json['description']} | Logged at: Now"
      else
        "#{json['label']} - #{json['description']} | Logged at: #{Time.parse(json['logged_at']).strftime("%m %b %y %I:%M%P")}"
      end
    end
  end
end
