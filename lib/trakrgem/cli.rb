require 'thor'

require 'trakrgem'
require 'trakrgem/version'

module Trakrgem
  class CLI < Thor

    desc 'login', 'Login to use Trakr'
    def login
      ::Trakrgem::Setup.login
    end

    desc 'last', 'Returns the last activity logged'

    def last
      ::Trakrgem::Activity.last
    end

    desc 'list', 'Returns the past 10 activities logged'
    def list
      ::Trakrgem::Activity.list
    end

    desc 'post', 'Create a new activity'
    def post(label, desc)
      ::Trakrgem::Activity.post(label, desc)
    end
  end
end
