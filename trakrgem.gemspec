# coding: utf-8
$:.push File.expand_path('../lib', __FILE__)
require 'trakrgem/version'

Gem::Specification.new do |spec|
  spec.name          = "trakrgem"
  spec.version       = Trakrgem::VERSION
  spec.authors       = ["Aaron Miler"]
  spec.email         = ["aaron@pineworks.io"]
  spec.summary       = %q{A gem to interface with Trakr (working title)}
  spec.description   = %q{}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files = `git ls-files`.split("\n")
  spec.executables   = ['trakrgem']
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'thor', '~> 0.19'
  spec.add_dependency 'typhoeus', '~> 0.6'
  spec.add_dependency 'highline', '~> 1.6'
  spec.add_dependency 'json', '~> 1.8'
  spec.add_dependency 'table_print', '~> 1.5'
  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
end
